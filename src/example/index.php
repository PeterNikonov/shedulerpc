<?php

session_start();
include './vendor/autoload.php';

use \MyPractic\Filef;
use \MyPractic\Stringf;
use \Shedule\Shedule;
use \Shedule\SheduleGui;
use \Shedule\Storage;
use Shedule\SheduleCalendar;

    $sg = new SheduleGui(Storage::getInstance());
    $sc = new SheduleCalendar(date('Y'), date('m'), Storage::getInstance()->date);

    $array = [
        'random' => time(),
        'SHEDULE_PERIOD' => $sg->shedulePeriod(),
        'DOCTOR_SELECT' => $sg->selectProperty('doctor', ['Куренков', 'Антонов']),
        'DIVISION_SELECT' => $sg->selectProperty('division', ['Тверская', 'Павелецкая']),
        'PROFILE_SELECT' => $sg->selectProperty('profile', ['Терапевт', 'Хирург']),
        'SHEDULE_CALENDAR' => $sc->Draw(),
        'HOURS_BY_DEFAULT' => $sg-> hoursByDefault(),
        'HOURS_TO_WEEKDAY' => $sg->hoursToWeekday(),
        'HOURS_TO_DATE' => $sg->hoursToDate(),
        'WEEKDAY_LINE' => $sg->weekDayLine()
    ];

    print Stringf::k2v(Filef::Read('../html/set.html'), $array);

    