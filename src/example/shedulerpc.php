<?php

session_start();
include './vendor/autoload.php';

use JsonRPC\Server;
use Shedule\Procedure;
use Shedule\Twin;

$server = new \JsonRPC\Server();

$procedureHandler = $server->getProcedureHandler();
$procedureHandler->withObject(new Procedure());
$procedureHandler->withObject(new Twin());

echo $server->execute();
