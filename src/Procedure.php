<?php

namespace Shedule;

/**
 * Реагирует на jsonrpc запросы
 */
class Procedure {

    /** установить одиночное свойство */
    public function changeSheduleProperty($property, $value) {
        Storage::setInstance(Storage::getInstance()->addValue($property, $value));
    }

    public function addSheduleItem($property, $value) {
        Storage::setInstance(Storage::getInstance()->addItem($property, $value));
    }

    public function removeSheduleItem($property, $value) {
        Storage::setInstance(Storage::getInstance()->removeItem($property, $value));
    }

    /** установить ассоциированное свойство */
    public function setSheduleAssocItem($property, $assoc, $value) {
        
        Storage::setInstance(Storage::getInstance()->addAssocItem($property, $assoc, $value));
    }

    // рендер дней недели
    public function renderWeekday() {
        $sg = new SheduleGui(Storage::getInstance());
        return $sg->weekDayLine();
    }

    // рендер календаря
    public function renderCalendar($y, $m) {
        $sc = new SheduleCalendar($y, $m, Storage::getInstance()->date);
        return $sc->Draw();
    }
    
    /** рендер часов для дней недели
     * @return string
     */
    public function renderHoursToWeekday() {
        $sg = new SheduleGui(Storage::getInstance());
        return $sg->hoursToWeekday();
    }
    /** рендер часов для дат
     * @return string
     */
    public function renderHoursToDate() {
        $sg = new SheduleGui(Storage::getInstance());
        return $sg->hoursToDate();
    }
    
}
