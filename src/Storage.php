<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Shedule;
use Shedule\Shedule;

class Storage {
    /**
     * 
     * @param Shedule $shedule
     */
    public static function setInstance( $shedule) {
        $_SESSION['shedule'] = serialize($shedule);
    }
    /**
     * 
     * @return Shedule
     */
    public static function getInstance() {

        $shedule = (isset($_SESSION['shedule'])) ? unserialize($_SESSION['shedule']) : new Shedule;
        return $shedule;

    }
}
