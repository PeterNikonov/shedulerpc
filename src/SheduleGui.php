<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Shedule;

use Shedule\Shedule;
use MyPractic\Datef;

class SheduleGui {

    /**
     *
     * @var Shedule
     */
    protected $shedule;

    public function __construct(Shedule $shedule) {
        $this->shedule = $shedule;
    }

    /**
     * Отдает html код плашки с днями
     * @param array $values
     * @return string
     */
    public function weekDayLine() {
        $values = $this->shedule->weekday;
        if (Shedule::WEEKDAY) {
            $path = '<div class="btn-group">';
            foreach (Shedule::WEEKDAY as $en => $ru) {
                $onclick = (@in_array($en, $values)) ? 'remove' : 'add';
                $btn_info = (@in_array($en, $values)) ? 'btn-info' : 'btn-default';
                $path.='<button type="button" class="btn ' . $btn_info . ' " onclick="changeWeekday(\'' . $onclick . '\', \'' . $en . '\');">' . $ru . '</button>';
            }
            $path.='</div>';
            return $path;
        } else {
            return 'No weekday array';
        }
    }

    public function hoursByDefault() {

        $startTime = $this->shedule->startTime;
        $endTime = $this->shedule->endTime;
        $duration = $this->shedule->duration;

        $path = '<ul class="list-group">
            <li class="list-group-item">
                    ' . $this->timeSet('<i class="glyphicon glyphicon-time"></i>', '*') . '
                    </li>
                </ul>';
        return $path;
    }

    protected function sortDateArray($array) {

        if (isset($array)) {
            foreach ($array as $date) {
                $key = (int) str_replace('-', '', $date);
                $mod[$key] = $date;
            }
        }

        ksort($mod);
        return array_values($mod);
    }

    public function hoursToDate() {

        $ret = '';

        if (isset($this->shedule->date) and ! empty($this->shedule->date)) {
            $array = $this->sortDateArray($this->shedule->date);
            foreach ($array as $date) {
                $ret.=$this->timeSet(Datef::RU_dateShortMonth($date), $date);
            }
            return '<ul class="list-group"><li class="list-group-item">' . $ret . '</li></ul>';
        } else {
            return '&nbsp;';
        }
    }

    protected function timeSet($name, $key) {

        $startTime = $this->shedule->startTime;
        $endTime = $this->shedule->endTime;
        $duration = $this->shedule->duration;

        return '
    <table class="table-condensed">
    <tr><td><span class="btn btn-sm btn-info">' . $name . '</span> <td>
        <td><input class="form-control" placeholder="ОТ" type="time" onchange="setSheduleAssocItem(\'startTime\', \'' . $key . '\', this.value);" value="' . $startTime[$key] . '"><td>
        <td><input class="form-control" placeholder="ДО" type="time" onchange="setSheduleAssocItem(\'endTime\', \'' . $key . '\', this.value);" value="' . $endTime[$key] . '"><td>
        <td><input class="form-control" placeholder="ИНТЕРВАЛ" type="number" onchange="setSheduleAssocItem(\'duration\', \'' . $key . '\', this.value);" value="' . $duration[$key] . '"><td>
    </tr>
    </table>';
    }

    public function hoursToWeekday() {

        $weekday = $this->shedule->weekday;
        $startTime = $this->shedule->startTime;
        $endTime = $this->shedule->endTime;
        $duration = $this->shedule->duration;

        if (Shedule::WEEKDAY) {
            foreach (Shedule::WEEKDAY as $en => $ru) {
                if (@in_array($en, $weekday)) {
                    $ulContent.='
  <li class="list-group-item">
    ' . $this->timeSet($ru, $en) . '
</li>
';
                }
            }
        }
        $path = '
<ul class="list-group">
        ' . $ulContent . '    
        </ul>';
        return $path;
    }

    public function selectProperty($property, $list) {

        if (isset($list)) {
            foreach ($list as $id => $name) {
                $selected = ($id == $this->shedule->$property) ? "selected" : "";
                $ret.='<option ' . $selected . ' value="' . $id . '">' . $name . '</option>' . "\n\r";
            }
            return '<select class="form-control" onchange="changeSheduleProperty(\'' . $property . '\', this.value);">' . $ret . '</select>';
        }
    }

    public function shedulePeriod() {

        return '
            <div class="form-group">
            <label class="col-sm-1 control-label">От</label>
                            <div class="col-sm-5">
                                <input type="date" class="form-control" value="' . $this->shedule->startDate . '" onchange="changeSheduleProperty(\'startDate\', this.value);">
                            </div>                
<label class="col-sm-1 control-label">До</label>
                            <div class="col-sm-5">
                                <input type="date" class="form-control" value="' . $this->shedule->endDate . '" onchange="changeSheduleProperty(\'endDate\', this.value);">
                            </div>
                            </div>
                            ';
    }

}
