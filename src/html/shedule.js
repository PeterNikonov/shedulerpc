
$.ajaxSetup({async: false});

function test(response) {
    if (response.result) {
        console.log(response.result);
    }
}

function JsonRequest(url, request, callback) {
    $.post(url, JSON.stringify(request), callback, "json");
}

function changeSheduleProperty(property, value) {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "changeSheduleProperty";
    request.params = [property, value];
    request.id = 1;
    request.jsonrpc = "2.0";

    response = JsonRequest(url, request, test);

}

function changeDate(date, method) {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = method + "SheduleItem";
    request.params = ['date', date];
    request.id = 3;
    request.jsonrpc = "2.0";
    console.log(request);
    response = JsonRequest(url, request, test);
    // рендер календаря
    de = date.split('-'),
    renderCalendar(de[0], de[1]);
    renderHoursToDate();
}

// установить день недели и обновить фрагмент
function changeWeekday(method, value) {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = method + "SheduleItem";
    request.params = ['weekday', value];
    request.id = 2;
    request.jsonrpc = "2.0";

    response = JsonRequest(url, request, test);
    renderWeekday();
    renderHoursToWeekday();
}

function renderWeekday() {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "renderWeekday";
    request.params = [];
    request.id = 2;
    request.jsonrpc = "2.0";

    $.post(url, JSON.stringify(request), function (response) {
        if (response.result) {
            $('#weekdayArea').html(response.result)
        }
    }, "json");
}

function renderCalendar(y, m) {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "renderCalendar";
    request.params = [y, m];

    request.id = 1;
    request.jsonrpc = "2.0";
    $.post(url, JSON.stringify(request), function (response) {
        if (response.result) {
            $("#calendarArea").html(response.result);
        }
    }, "json");
}

function setSheduleAssocItem(property, key, $value) {
    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "setSheduleAssocItem";
    request.params = [property, key, $value];

    request.id = 3;
    request.jsonrpc = "2.0";
    console.log(request.method);
    response = JsonRequest(url, request, test);
}

function renderHoursToWeekday() {

    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "renderHoursToWeekday";
    request.params = [];
    request.id = 2;
    request.jsonrpc = "2.0";

    $.post(url, JSON.stringify(request), function (response) {
        if (response.result) {
            $('#hoursToWeekdayArea').html(response.result)
        }
    }, "json");
}

function renderHoursToDate() {
    
    var url = "http://clean/shedulerpc.php";
    var request = {}

    request.method = "renderHoursToDate";
    request.params = [];
    request.id = 2;
    request.jsonrpc = "2.0";

    $.post(url, JSON.stringify(request), function (response) {
        if (response.result) {
            $('#hoursToDateArea').html(response.result)
        }
    }, "json");
}
