<?php

namespace Shedule;

use MyPractic\Datef;

// реализация  календаря для управления приемными днями
class SheduleCalendar {

    private $year;
    private $month;
    private $dateArray;

    public function __construct($year, $month, $dateArray) {

        $this->year = (int) $year;
        $this->month = (int) $month;
        $this->dateArray = $dateArray;
    }

    public function Draw() {

        $month_stamp = mktime(0, 0, 0, $this->month, 1, $this->year);
        $day_count = date("t", $month_stamp);
        $weekday = date("w", $month_stamp);

        $fd = Datef::Ymd($this->year . "-" . $this->month . "-01");

        if ($weekday == 0) {
            $weekday = 7;
        }

        $start = -($weekday - 2);
        $last = ($day_count + $weekday - 1) % 7;

        $end = ($last == 0) ? $day_count : $day_count + 7 - $last;
        $prev_exp = Datef::date_explode(Datef::date_to_period($fd, '-', 1, 'm'));
        $next_exp = Datef::date_explode(Datef::date_to_period($fd, '+', 1, 'm'));

        $prevYear = $prev_exp[0];
        $prevMonth = $prev_exp[1];
        $nextYear = $next_exp[0];
        $nextMonth = $next_exp[1];

        $ret = '
<table class="">
<tr>
<td><span class="btn btn-default" onclick="renderCalendar(' . $prevYear . ', ' . $prevMonth . ');">
<i class="glyphicon glyphicon-chevron-left color_primary"></i>
</span></td>
<td colspan="5">
<button type="button" class="btn btn-block">
' . Datef::rus_im($this->month) . '&nbsp;' . $this->year . '  
</button>
</td>
<td>
<span class="btn btn-default" onclick="renderCalendar(' . $nextYear . ', ' . $nextMonth . ');">
<i class="glyphicon glyphicon-chevron-right color_primary"></i>
</span></td>
</tr>
<tr class="thead">
<td>Пн</td>
<td>Вт</td>
<td>Ср</td>
<td>Чт</td>
<td>Пт</td>
<td>Сб</td>
<td>Вс</td>
</tr>
';

        $i = 0;
        for ($d = $start; $d <= $end; $d++) {

            if (!($i++ % 7)) {
                $ret.='<tr>';
            }
            if ($d < 1 OR $d > $day_count) {
                $cell_content = "&nbsp";
            } else {
                $current_date = Datef::Ymd($this->year . '-' . $this->month . '-' . $d);
                if (is_array($this->dateArray) AND in_array($current_date, $this->dateArray)) {
                    $btnClass = 'btn-info';
                    $method = 'remove';
                } else {
                    $btnClass = 'btn-default';
                    $method = 'add';
                }
                $cell_content = '<span class="btn ' . $btnClass . ' btn-block btn-sm" onclick="changeDate(\'' . $current_date . '\', \'' . $method . '\');">' . $d . '</span>';
            }
            $ret.='<td>' . $cell_content . '</td>';
            if (!($i % 7)) {
                $ret.='</tr>';
            }
        }
        $ret.='</table>';
        return $ret;
    }
}

?>